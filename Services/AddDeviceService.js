
'use strict';
import ReactNativeRouter from './../NativeModules';
import {Platform} from 'react-native';
import {NativeModules} from 'react-native';
import base64 from 'react-native-base64' 
import Account from '../Utils/Account';
export default class AddDeviceService{
    
    static initializeDeviceRegistrationWithCompletionHandler(successBlock,failBlock){
        
        if (Platform.OS === 'ios'){
            var reactNativeRouter = NativeModules.ReactNativeRouter;
            reactNativeRouter.initializeDeviceRegistrationWithCompletionHandler((responseString,error) => {
                if (error) {
                    failBlock(error);
                  } else {
                    successBlock(responseString);
                  }
                });
        }else{ // In case of Android
            ReactNativeRouter.initializeDeviceRegistrationWithCompletionHandler((responseString) => {
                successBlock(responseString);
            },  (error) => {
                failBlock(error);
            });
        } 
    }

    static completeDeviceRegistrationForAccount(tenantAccountId,inputString,successBlock,failBlock){
        console.log('INPUT', inputString)
        Account.getSharedInstance().setNickname(JSON.parse(base64.decode(inputString)).deviceNickname);    
        
        var reactNativeRouter = NativeModules.ReactNativeRouter;
        if (Platform.OS === 'ios'){
            reactNativeRouter.completeDeviceRegistrationForAccount(tenantAccountId,inputString,(responseString,error) => {
                if (error) {
                    console.log('ERROR', error)
                    failBlock(error);
                  } else {
                      console.log('RESPONSE', responseString)
                    successBlock(responseString);
                  }
                });
        
        }else{ // In case of Android
            ReactNativeRouter.completeDeviceRegistrationForAccount(tenantAccountId,inputString,(responseString) => {
                successBlock(responseString);
            },  (error) => {
                failBlock(error);
            });
        } 
    }
}  