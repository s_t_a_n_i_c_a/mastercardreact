'use strict';
import ReactNativeRouter from './../NativeModules';
import {Platform} from 'react-native';
import {NativeModules} from 'react-native';

export default class EnrollmentService{

    static initializeFidoRegistration(successBlock,failBlock){

        var reactNativeRouter = NativeModules.ReactNativeRouter;
        if (Platform.OS === 'ios'){
            console.log('ios')
              reactNativeRouter.initializeFidoRegistration((responseString,error) => {
                  console.log(responseString, error)
                if (error) {
                    failBlock(error);
                  } else {
                    successBlock(responseString);
                  }
                });

        }else{ // In case of Android
            ReactNativeRouter.initializeFidoRegistration((responseString) => {
                successBlock(responseString);
            },  (error) => {
                failBlock(error);
            });
        } 
    }

    static processFidoRegistration(encryptedString,successBlock,failBlock){
        var reactNativeRouter = NativeModules.ReactNativeRouter;
        if (Platform.OS === 'ios'){
            console.log('processing', encryptedString)
            reactNativeRouter.processFidoRegistration(encryptedString,(response,error) => {
                console.log('fido results', error, response)
                if (error) {
                    failBlock(error);
                  } else if(response) {
                    successBlock(response);
                  }else{
                    failBlock(new Error('Cancelled'));
                  }
                });

        }else{
            ReactNativeRouter.processFidoRegistration(encryptedString,(responseString) => {
                successBlock(responseString);
            },  (error) => {
                failBlock(error);
            });
        } 
    }

    static completeFidoRegistration(serverResponse,successBlock,failBlock){
        var reactNativeRouter = NativeModules.ReactNativeRouter;
        if (Platform.OS === 'ios'){
            reactNativeRouter.completeFidoRegistration(serverResponse,(responseString,error) => {
                if (error) {
                    failBlock(error);
                  } else {
                    successBlock(responseString);
                  }
            });
        }else{ // In case of Android
            ReactNativeRouter.completeFidoRegistration(serverResponse,(responseString) => {
                successBlock(responseString);
            },  (error) => {
                failBlock(error);
            });
        } 
    }

    static setupPushNotificationPayload(messageData,successBlock,failBlock){
        var reactNativeRouter = NativeModules.ReactNativeRouter;
        var channelId = JSON.stringify(messageData.channelId);
        var serverRandomData = JSON.stringify(messageData.serverRandomData);
        console.log('CHANNEL',channelId)
        channelId = channelId.replace(/^"(.*)"$/, '$1');
        serverRandomData = serverRandomData.replace(/^"(.*)"$/, '$1');


        if (Platform.OS === 'ios'){
            reactNativeRouter.setupPushNotificationPayload({'channelId':channelId,'serverRandomData':serverRandomData},(responseString,error) => {
                if (error) {
                    failBlock(error);
                  } else {
                    successBlock(responseString);
                  }
                });
        }else{ // In case of Android
             ReactNativeRouter.setupPushNotificationPayload(channelId,serverRandomData,'SECURE_DEVICE_VERSION_2');
        } 
    }
}  
