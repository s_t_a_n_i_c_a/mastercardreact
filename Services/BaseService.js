
import { BASE_URL_REF_BACKEND } from '.././AppConstants';

export default class BaseService{

     request(serviceName,method,headerParams,bodyParams,successBlock,failBlock) {
         fetch(`${BASE_URL_REF_BACKEND}/${serviceName}`,{
                method: method,
                headers: headerParams,
                body: bodyParams,
            })
            .then((response) => response.text())
            .then((responseString) => {
                console.log('response is', responseString)
                successBlock(responseString);
            }).catch((error) => {
                console.log('base service error', error)
                failBlock(error);
        })
        .done();
     }
  }

  
