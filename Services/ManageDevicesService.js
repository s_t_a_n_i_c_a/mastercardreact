'use strict';
import ReactNativeRouter from '../NativeModules';
import {Platform} from 'react-native';
import {NativeModules} from 'react-native';

export default class ManageDevicesService{

    static initializeListDevices(successBlock,failBlock){
        var reactNativeRouter = NativeModules.ReactNativeRouter;
        if (Platform.OS === 'ios'){
              reactNativeRouter.initializeListDevices((responseString,error) => {
                if (error) {
                    failBlock(error);
                  } else {
                    successBlock(responseString);
                  }
                });

        }else{ // In case of Android
            ReactNativeRouter.initializeListDevices((responseString) => {
                successBlock(responseString);
            },  (error) => {
                failBlock(error);
            });
        } 
    }


    static completeListDevices(listDevicesResponse,successBlock,failBlock){
        var reactNativeRouter = NativeModules.ReactNativeRouter;
        if (Platform.OS === 'ios'){
          console.log('list devices response', listDevicesResponse)
              reactNativeRouter.completeListDevices(listDevicesResponse,(responseString,error) => {
                if (error) {
                    failBlock(error);
                  } else {
                    successBlock(responseString);
                  }
                });

        }else{ // In case of Android
            ReactNativeRouter.completeListDevices(listDevicesResponse,(responseString) => {
                successBlock(responseString);
            },  (error) => {
                failBlock(error);
            });
        } 
    }

    static initializeDisableDevices(deviceNicknames,successBlock,failBlock){
      var reactNativeRouter = NativeModules.ReactNativeRouter;
      if (Platform.OS === 'ios'){
            reactNativeRouter.initializeDisableDevices(deviceNicknames,(responseString,error) => {
              if (error) {
                  failBlock(error);
                } else {
                  successBlock(responseString);
                }
              });

      }else{ // In case of Android
          ReactNativeRouter.initializeDisableDevices(deviceNicknames,(responseString) => {
              successBlock(responseString);
          },  (error) => {
              failBlock(error);
          });
      } 
  }


  static completeDisableDevices(disableDevicesResponse,successBlock,failBlock){
      var reactNativeRouter = NativeModules.ReactNativeRouter;
      if (Platform.OS === 'ios'){
            reactNativeRouter.completeDisableDevices(disableDevicesResponse,(responseString,error) => {
              if (error) {
                  failBlock(error);
                } else {
                  successBlock(responseString);
                }
              });

      }else{ // In case of Android
          ReactNativeRouter.completeDisableDevices(disableDevicesResponse,(responseString) => {
              successBlock(responseString);
          },  (error) => {
              failBlock(error);
          });
      } 
  }

  static deleteAccount(){
    var reactNativeRouter = NativeModules.ReactNativeRouter;
    if (Platform.OS === 'ios'){
          reactNativeRouter.deleteAccount();
    }else{ // In case of Android
        ReactNativeRouter.deleteAccount();
    } 
}

}