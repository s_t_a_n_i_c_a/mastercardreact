'use strict'
import React from 'react'
import { Platform,AsyncStorage } from 'react-native'
import { createAppContainer, createStackNavigator } from 'react-navigation'
import { EventRegister } from 'react-native-event-listeners'
import { BASE_URL_MIS } from './AppConstants'

import AddDevice from './Screens/AddDevice'
import VerifyDevice from './Screens/VerifyDevice'
import Enrollment from './Screens/Enrollment'
import Menu from './Screens/Menu'
import ManageDevices from './Screens/ManageDevices'

import Account from './Utils/Account'
import ReactNativeRouter from './NativeModules'
import PushNotificationIOS from "@react-native-community/push-notification-ios";
//import firebase from 'react-native-firebase'

//Flow types
import type { RemoteMessage } from 'react-native-firebase';
import type { Notification } from 'react-native-firebase';

const RootStack = createStackNavigator(
    {
        AddDevice: {
            screen: AddDevice, navigationOptions: {
                title: 'Add Device',
                headerLeft: null
            },
        },
        VerifyDevice: {
            screen: VerifyDevice, navigationOptions: {
                title: 'Verify Device',
                headerLeft: null
            },
        },
        Enrollment: {
            screen: Enrollment, navigationOptions: {
                title: 'Enrollment Biometrics',
                headerLeft: null
            },
        },
        Menu: {
            screen: Menu, navigationOptions: {
                title: 'Menu',
                headerLeft: null
            },
        },
        ManageDevices: {
            screen: ManageDevices, navigationOptions: {
                title: 'List/Disable Device(s)',
                ////headerLeft: null
            },
        },
        
    },
    {
        initialRouteName: 'AddDevice',
    }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {

    constructor(props) {
        super(props);
    }
    componentWillUnmount() {
        if (Platform.OS === 'ios') {
            PushNotificationIOS.removeEventListener(type, handler);
        } else {
            this.messageListener();
        }
    }

    componentDidMount = () => {

        if (Platform.OS === 'ios') {
            ReactNativeRouter.setMISUrl(BASE_URL_MIS);
            this.requestPushNotificationAcess();

        } else {
             const channel = new firebase.notifications.Android.Channel(
            "com.mastercard.identitycheck.mybank.default",
             "General Notifications",
             firebase.notifications.Android.Importance.High
           ).setDescription("General Notifications")

          firebase.notifications().android.createChannel(channel);
            ReactNativeRouter.setMISUrl(BASE_URL_MIS);

            firebase.messaging().hasPermission()
                .then(enabled => {
                    if (!enabled) {
                        firebase1.messaging().requestPermission()
                            .then(() => {
                            })
                            .catch(error => {
                                alert("No push permissions");
                            });
                    }
                });


            firebase.messaging().getToken().then((deviceToken) => {
                Account.getSharedInstance().setPushToken(deviceToken);
                alert("App registered for remote push notifications");

            });

            firebase.messaging().onTokenRefresh((deviceToken) => {
                Account.getSharedInstance().setPushToken(deviceToken);
            });

            this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
                let messageData = message.data;
                this.processNotification(messageData);
            });
        }
    }

    requestPushNotificationAcess() {
        PushNotificationIOS.requestPermissions();

        PushNotificationIOS.addEventListener('register', (deviceToken) => {
            Account.getSharedInstance().setPushToken(deviceToken)
            console.log('push token', deviceToken)
            alert("App registered for remote push notifications");
        });

        PushNotificationIOS.addEventListener('notification', (notification) => {
            let messageData = notification.getData();
            this.processNotification(messageData);
        });

        PushNotificationIOS.addEventListener('registrationError', (error) => {
            alert(error);
        });
    }

    processNotification(messageData) {
        console.log('push received', messageData)
        var myPushType = JSON.stringify(messageData.pushType);
        if (myPushType === JSON.stringify('VERIFY_DEVICE')) {
            let verificationCode = JSON.stringify(messageData.verificationCode);
            console.log('verification code in app.js', messageData.verificationCode)
            Account.getSharedInstance().setVerificationCode(messageData.verificationCode);

            let nickname = JSON.stringify(messageData.deviceNickname);
            console.log('FEI SUX', nickname)
            Account.getSharedInstance().setNickname(messageData.deviceNickname);

            AsyncStorage.setItem('verificationCode',verificationCode);
            EventRegister.emit('VerifyDevicePayloadReceived', messageData);

        }else if (myPushType === JSON.stringify('MCSDKPayload')) {
            EventRegister.emit('MCPayloadReceived', messageData);
        }
    }
    render() {
        return <AppContainer />;
    }
}
