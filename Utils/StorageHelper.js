import {AsyncStorage} from 'react-native'

export default class StorageHelper{

    getDataForKey(key){
        AsyncStorage.getItem(key).then((value) =>{
            return value;
        })
    }

    setDataForKey = (key,value) => {
      AsyncStorage.setItem(key, value);
   }
}