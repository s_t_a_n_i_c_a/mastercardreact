import React, { Component } from 'react';
import { ActivityIndicator, View, Text, StyleSheet, Dimensions } from 'react-native';

export default class CustomLoader extends Component {
   state = { animating: true }

   closeActivityIndicator = () => {
      this.setState({animating: false})
   }
   render() {
      const animating = this.state.animating
      return (

         <View style={styles.main}>
            <View style={styles.loaderContainer}>
               <ActivityIndicator
                  animating={animating}
                  color='#fff'
                  size="large"
                  style={styles.activityIndicator} />
               <Text style={styles.textStyle}>Please wait</Text>
            </View>
         </View>
      )
   }
}

const styles = StyleSheet.create({
   main: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      top: 0,
      backgroundColor: 'rgba(0,0,0,0)'
   },
   loaderContainer: {
      position: 'absolute',
      top: (Dimensions.get('window').height / 2) - 120,
      left: (Dimensions.get('window').width / 2) - 60,
      right: 0,
      bottom: 0,
      justifyContent: 'center',
      alignItems: 'center',
      height: 120, width: 120,
      borderRadius: 20,
      backgroundColor: 'rgba(1,1,1,0.8)'
   },
   activityIndicator: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
   },
   textStyle: { 
      flex: 0.5,
      marginTop: 0,
      fontSize:16,
      color:'#fff'
   },
})