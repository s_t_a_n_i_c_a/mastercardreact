export default class Account {

    static sharedInstance = null;

    _pushToken = "";
    _nickname  = "";
    _verificationCode = "";
    _tenantAccountId  = "";

    /**
     * @returns {Account}
     */
    static getSharedInstance() {
        if (Account.sharedInstance == null) {
            Account.sharedInstance = new Account();
        }
        return this.sharedInstance;
    }

    getPushToken() {
        return this._pushToken.replace(/^"(.*)"$/, '$1');
    }
    setPushToken(token) {
        this._pushToken = token;
    }

    getNickname() {
        return this._nickname.replace(/^"(.*)"$/, '$1');
    }
    setNickname(nickname) {
        this._nickname = nickname;
    }

    getVerificationCode() {
        // return "abc123";
        console.log('verification code:', this._verificationCode);
        return this._verificationCode.replace(/^"(.*)"$/, '$1');
    }
    setVerificationCode(verificationCode) {
        this._verificationCode = verificationCode;
    }

    getTenantAccountId() {
        return this._tenantAccountId.replace(/^"(.*)"$/, '$1');;
    }
    setTenantAccountId(tenantAccountId) {
        this._tenantAccountId = tenantAccountId;
    }
}