//
//  ReactNativeRouter.h
//  ReferenceAppReactN
//
//  Created by Ghorpade, Krupal on 13/02/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import <React/RCTBridgeModule.h>

NS_ASSUME_NONNULL_BEGIN


@interface ReactNativeRouter : NSObject<RCTBridgeModule>
@end

NS_ASSUME_NONNULL_END
