'use strict';
import React from 'react';
import {StyleSheet, View, Text, Button, Keyboard,Platform } from 'react-native';
import CustomLoader from '../Utils/CustomLoader'
import Account from "../Utils/Account";
import VerifyDeviceService from '../Services/VerifyDeviceService';
import ReactNativeRouter from './../NativeModules';
import {NativeModules} from 'react-native';
import {AsyncStorage} from 'react-native'

export default class VerifyDevice extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: false, nickname: '' };
  }

  componentDidMount = () => {
    AsyncStorage.getItem('verificationCode').then((value) =>{
      console.log('verification code in verify device', value)
        Account.getSharedInstance().setVerificationCode(value);
    });

    var reactNativeRouter = NativeModules.ReactNativeRouter;
    if (Platform.OS === 'ios'){
        reactNativeRouter.getNickname((nickname,error) => {
          this.setState({ nickname:nickname });
          Account.getSharedInstance().setNickname(nickname);
        });

    }else{ 
      ReactNativeRouter.getNickname((nickname)=>{
        this.setState({ nickname:nickname });
        Account.getSharedInstance().setNickname(nickname);
        },(error)=>{});
    }
  }

  verifyDevice = () => {
    console.log('verifying device')
    let nickname = Account.getSharedInstance().getNickname();
    let verificationCode = Account.getSharedInstance().getVerificationCode();
    console.log('ASDASDASDAS')
    console.log(nickname, verificationCode)
    nickname = nickname || 'Rob'
    if (nickname == "") {
      alert("Nickname isn't available")
      return
    }if (verificationCode == "") {
      alert("Verification Code isn't available")
      return
    }
    Keyboard.dismiss();
    this.setState({ isLoading: true });
    console.log('calling verify service')
    VerifyDeviceService.verifyDevice(nickname, verificationCode, (response) => {
      console.log(response);
      this.setState({ isLoading: false })
      alert("Device Verified Successfully");
      console.log(this.props.navigation)
      this.props.navigation.replace('Enrollment');
    }, (error) => {
      this.setState({ isLoading: false })
      alert(error);
    });
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        <View style={styles.MainContainer}>

          <Text style={styles.HeaderStyle}>Your nickname is: {this.state.nickname}</Text>
          <Text style={styles.HeaderStyle}>Please Proceed to Verify Your Device</Text>
          <View style={styles.VerifyDeviceButtonStyle}>
            <Button
              title="Verify Device" 
              color="#fe8f0a"
              onPress={this.verifyDevice}
            />
          </View>
        </View>
        {
          this.state.isLoading ? <CustomLoader /> : null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  HeaderStyle: { position: 'relative', top: 80, alignSelf: 'center', color: '#000000' },
  VerifyDeviceButtonStyle: { position: 'absolute', bottom: 100, alignSelf: 'center' },
  MainContainer: { flex: 1, backgroundColor: '#FFFFFF', },
});