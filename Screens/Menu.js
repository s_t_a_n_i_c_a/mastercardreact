'use strict';

import React from 'react'
import { StyleSheet, View, Text, Button,Alert } from 'react-native'
import CustomLoader from '../Utils/CustomLoader'
import AuthenticationService from './../Services/AuthenticationService'
import { EventRegister } from 'react-native-event-listeners'
import EnrollmentService from '../Services/EnrollmentService';
import BaseService from './../Services/BaseService'
import {TENANT_CODE } from './../AppConstants';

export default class Menu extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: false };
  }
  componentWillMount() {
    this.listener = EventRegister.addEventListener('MCPayloadReceived', (messageData) => {
      EnrollmentService.setupPushNotificationPayload(messageData,(response) =>{
      },(error) =>{
        alert(error);
      });
    })
}
componentWillUnmount() {
  EventRegister.removeEventListener(this.listener)
}
  authenticateInBandLowPolicy = () => {

    this.setState({ isLoading: true });

    const uuidv1 = require('uuid/v1');
    var randomNumber = uuidv1();
    AuthenticationService.initializeFidoAuthentication(randomNumber,'low','MOBILE_BANKING','',(response) => {
      this.setState({ isLoading: false });
      this.callInitializeFidoAuthenticationAPI(response);
    },(error) => {
      this.setState({ isLoading: false });
      alert(error)
    })
  }

  authenticateInBandMediumPolicy = () =>{

    this.setState({ isLoading: true });

    const uuidv1 = require('uuid/v1');
    var randomNumber = uuidv1();
    AuthenticationService.initializeFidoAuthentication(randomNumber,'medium','MOBILE_BANKING','',(response) => {
      this.setState({ isLoading: false });
      this.callInitializeFidoAuthenticationAPI(response);
    },(error) => {
      this.setState({ isLoading: false });
      alert(error)
    })

  }

  manageDevices = () =>{
    this.props.navigation.push('ManageDevices');
  }

  callInitializeFidoAuthenticationAPI(responseString){
        
    this.setState({ isLoading: true });
    var headerParams = { 'Accept': 'application/json', 'Content-Type': 'application/json', 'charset': 'utf-8','tenantCode':TENANT_CODE };
    var bodyParams = responseString;
            
    new BaseService().request("mbPBES", "POST", headerParams, JSON.stringify({'payload': {'requestPBESstring':bodyParams}}), (serverResponse) => {
      this.setState({ isLoading: false });
        
      AuthenticationService.processFidoAuthentication(serverResponse,(clientResponse)=>{
          this.callCompleteFidoAuthenticationAPI(clientResponse);
        },(error)=>{
          alert(error);
        });      
        
    }, (error) => {   
        this.setState({ isLoading: false });
        alert(error);
    });
}


callCompleteFidoAuthenticationAPI = (clientResponse) => {
  
  this.setState({ isLoading: true });

      var headerParams = { 'Accept': 'application/json', 'Content-Type': 'application/json', 'charset': 'utf-8','tenantCode':TENANT_CODE };
      var bodyParams = clientResponse;       
      new BaseService().request("mbPBES", "POST", headerParams, JSON.stringify({'payload': {'requestPBESstring':bodyParams}}), (serverResponse) => {
        AuthenticationService.completeFidoAuthentication(serverResponse,(clientResponse)=>{
          this.setState({isLoading: false })
          alert(clientResponse);
        },(error)=>{
          this.setState({isLoading: false })
          alert(error);
        });
      }, (error) => {   
        alert(error);
        this.setState({isLoading: false })
      });
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        
        <Text style={styles.HeaderStyle}>You've Enrolled Successfully!</Text>

        <View style={styles.ButtonStyle1}>
          <Button
            title="In-Band Authentication - Ex.1"
            color="#fe8f0a"
            onPress={this.authenticateInBandLowPolicy}
          />
        </View>
        <View style={styles.ButtonStyle2}>
          <Button
            title="In-Band Authentication - Ex.2"
            color="#fe8f0a"
            onPress={this.authenticateInBandMediumPolicy}
          />
        </View>
        <View style={styles.ButtonStyle3}>
          <Button
            title="Manage Devices"
            color="#fe8f0a"
            onPress={this.manageDevices}
          />
        </View>
        {
          this.state.isLoading ? <CustomLoader /> : null
        }
      </View>
    );
  }

}

const styles = StyleSheet.create({
  ButtonStyle1: {position: 'absolute', bottom: 160, alignSelf: 'center'},
  ButtonStyle2: {position: 'absolute', bottom: 100, alignSelf: 'center'},
  ButtonStyle3: {position: 'absolute', bottom: 40, alignSelf: 'center'},
  HeaderStyle: { position: 'relative', top: 80, alignSelf: 'center', color: '#000000' },
  MainContainer: { flex: 1, backgroundColor: '#FFF', },

});