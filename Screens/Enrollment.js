'use strict';
import React from 'react';
import {StyleSheet, View, Text, Button} from 'react-native';
import CustomLoader from '../Utils/CustomLoader'
import EnrollmentService from '../Services/EnrollmentService';
import { EventRegister } from 'react-native-event-listeners'
import BaseService from './../Services/BaseService'
import {TENANT_CODE } from './../AppConstants';

export default class Enrollment extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: false };
  }

  componentWillMount() {
        this.listener = EventRegister.addEventListener('MCPayloadReceived', (messageData) => {
          EnrollmentService.setupPushNotificationPayload(messageData,(response) =>{
          },(error) =>{
            alert(error);
          });
        })
    }
    
    componentWillUnmount() {
        EventRegister.removeEventListener(this.listener)
    }

enrollBiometrics = () => {
    this.setState({ isLoading: true });
    
      EnrollmentService.initializeFidoRegistration((responseString) => {
          this.setState({ isLoading: false });
          this.callInitializeFidoRegistrationAPI(responseString);
      },(error)=>{
        console.log('fail')
          this.setState({ isLoading: false });
          alert(error);
      });

  }

 callInitializeFidoRegistrationAPI(responseString){
        
        this.setState({ isLoading: true });
        var headerParams = { 'Accept': 'application/json', 'Content-Type': 'application/json', 'charset': 'utf-8','tenantCode':TENANT_CODE };
        var bodyParams = responseString;
        new BaseService().request("mbPBES", "POST", headerParams, JSON.stringify({'payload': {'requestPBESstring':bodyParams}}), (serverResponse) => {
          console.log('Fido registration', JSON.stringify(JSON.parse(serverResponse).payload.responsePayload))
          this.setState({ isLoading: false });
            EnrollmentService.processFidoRegistration(JSON.stringify(JSON.parse(serverResponse).payload.responsePayload),(clientResponse)=>{
              console.log('client response is', clientResponse)
              this.callCompleteFidoRegistrationAPI(clientResponse);

            },(error)=>{
              console.log('some error', error)
              alert(error);
            });      
            
        }, (error) => {   
            this.setState({ isLoading: false });
            alert(error);
        });
    }

callCompleteFidoRegistrationAPI(clientResponse){
       this.setState({ isLoading: true });

        var headerParams = { 'Accept': 'application/json', 'Content-Type': 'application/json', 'charset': 'utf-8','tenantCode':TENANT_CODE };
                var bodyParams = clientResponse;
                console.log(bodyParams)
                new BaseService().request("mbPBES", "POST", headerParams, JSON.stringify({'payload': {'requestPBESstring':bodyParams}}), (serverResponse) => {
                  this.setState({ isLoading: false });
                  EnrollmentService.completeFidoRegistration(JSON.stringify(JSON.parse(serverResponse).payload.responsePayload),(clientResponse)=>{
                    alert("Enrolled Successfully");
                    this.props.navigation.replace('Menu');

                  },(error)=>{
                    alert(error);
                  });
                    
                }, (error) => {   
                    this.setState({ isLoading: false });
                    alert(error);
                });
    }

 render() {
    return (
      <View style={styles.MainContainer}>
        <View style={styles.MainContainer}>

          <Text style={styles.HeaderStyle}>Device Verified Successfully!</Text>
          <Text style={styles.HeaderStyle}>Please Proceed for Biometrics Enrollment</Text>
          <View style={styles.EnrollButtonStyle}>
            <Button
              title="Enroll Biometrics"
              color="#fe8f0a"
              onPress={this.enrollBiometrics}
            />
          </View>
        </View>
        {
          this.state.isLoading ? <CustomLoader /> : null
        }
      </View>
    );
  }

  }
  const styles = StyleSheet.create({
    HeaderStyle: { position: 'relative', top: 80, alignSelf: 'center', color: '#000000' },
    EnrollButtonStyle: { position: 'absolute', bottom: 100, alignSelf: 'center' },
    MainContainer: { flex: 1, backgroundColor: '#FFFFFF', },
  });