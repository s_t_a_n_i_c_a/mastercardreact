'use strict';
import React, { Component } from 'react';
import { Platform, TextInput, StyleSheet, View, Text, Button,Keyboard,Alert} from 'react-native';
import BaseService from '../Services/BaseService';
import AddDeviceService from '../Services/AddDeviceService';
import CustomLoader from '../Utils/CustomLoader'
import Account from "../Utils/Account";
import ReactNativeRouter from './../NativeModules';
import {NativeModules} from 'react-native';
import { BASE_URL_MIS,TENANT_CODE } from './../AppConstants';
import { EventRegister } from 'react-native-event-listeners'

export default class AddDevice extends Component {
    constructor(props) {
        super(props);
        this.state = { accountId: '', nickname: '', isLoading: false};
    }

    componentWillUnmount() {
        EventRegister.removeEventListener(this.listener)
    }
    componentDidMount = () => {
        
        this.listener = EventRegister.addEventListener('VerifyDevicePayloadReceived', (messageData) => {
          this.showVerifyDeviceAlert(messageData);
        })

        var baseURL = `${BASE_URL_MIS}`;    

        var reactNativeRouter = NativeModules.ReactNativeRouter;
        if (Platform.OS === 'ios'){
            reactNativeRouter.getAccountStatus((accountStatus,error) => {
                if (accountStatus == '0'){}
                // this.props.navigation.replace('VerifyDevice');
                if (accountStatus == '1')
                this.props.navigation.replace('Enrollment');
                if (accountStatus == '2')
                this.props.navigation.replace('Menu');
            });
            reactNativeRouter.setMISUrl(baseURL);
            reactNativeRouter.registerAuthenticators();
            
        }else{ // In case of Android

            ReactNativeRouter.setMISUrl(baseURL);
            ReactNativeRouter.getAccountStatus((res) => {
                if(res == '1')
                    this.props.navigation.replace('VerifyDevice');
                if(res == '2')
                    this.props.navigation.replace('Enrollment');  
                if(res == '3')
                    this.props.navigation.replace('Menu');  
                
            },(error)=>{

            });
        }
    }

    callAddDevice = () => {
        let deviceToken = Account.getSharedInstance().getPushToken()
        const { accountId, nickname } = this.state

        if(accountId.trim()==""){
            alert("Please enter account id")
            return
        }
        if(nickname.trim()==""){
            alert("Please enter nickname")
            return
        }
        if(deviceToken.trim()==""){
            alert("Device token is nil")
            return
        }
        Keyboard.dismiss();
        this.setState({ accountId: accountId.trim()})
        this.setState({ nickname: nickname.trim()})
        this.setState({ isLoading: true })

        AddDeviceService.initializeDeviceRegistrationWithCompletionHandler((responseString) => {
            var headerParams = { 'Content-Type': 'application/json', 'charset': 'utf-8'};
            var bodyParams = { payload: {knowledgeFactor:'', activationCode:'', 'SDKRequest': responseString, 'pushToken': deviceToken.trim(), 'tenantAccountId': accountId.trim(), 'deviceNickname': nickname.trim(), 'phoneType': Platform.OS === 'ios'? 'iOS':'Android'}};
            console.log(bodyParams)
            new BaseService().request("mbAddDevice", "POST", headerParams, JSON.stringify(bodyParams), (result) => {
                console.log('add device response', result)
                AddDeviceService.completeDeviceRegistrationForAccount(accountId.trim(),JSON.parse(result).payload.SDKResponse,(responseString) => {
                    this.setState({ isLoading: false })
                    alert('Add Device Successful')
                    this.props.navigation.replace('VerifyDevice');
                },(error) => {
                    this.setState({ isLoading: false })
                    console.log('d')
                    alert(error);
                });
                
            }, (error) => {
                this.setState({ isLoading: false })
                console.log('e')
                alert(error);
            });

        }, (error) => {
            console.log('f')
            alert(error);
        });

    }

    showVerifyDeviceAlert(messageData){
        Alert.alert('Verify Device Push',JSON.stringify(messageData),
        [{text: 'Goto Verify Device Screen', onPress: () => this.props.navigation.replace('VerifyDevice')}],
        {cancelable: false});
    }

    render(){
        return (
            <View style={styles.MainContainer}>
                <View   style={styles.MainContainer}>

                    <Text style={styles.headerStyle}>Reference App - React Native</Text>

                    <View style={{ flex: 1, flexDirection: "column", }}>
                        <TextInput
                            style={{ marginLeft: 50, marginRight: 50, marginTop: 150, height: 44, borderWidth: 1, borderColor: 'black' }} placeholder="Enter Tenant Account ID"
                            onChangeText={(accountId) => this.setState({ accountId })}
                            onSubmitEditing={()=> this.nicknameTextInput.focus()}
                            value={this.state.acountId}
                            blurOnSubmit={false}
                        / >
                        
                        <TextInput
                            ref={(input) => { this.nicknameTextInput = input; }}
                            style={{ marginLeft: 50, marginRight: 50, marginTop: 44, height: 44, borderWidth: 1, borderColor: 'black' }} placeholder="Enter Device Nickname"
                            onChangeText={(nickname) => this.setState({ nickname })}
                            value={this.state.nickname}
                            onSubmitEditing={this.callAddDevice}
                            blurOnSubmit={false}
                        />
                        
                        <Text style={{ marginLeft: 50, marginRight: 50, marginTop: 10, height: 44 }}>Max. 20 alphanumeric characters, space, "_", "-" are permitted</Text>

                    </View>

                    <View style={styles.addDeviceButtonStyle}>
                        <Button
                            title="Add Device"
                            color="#fe8f0a"
                            onPress={this.callAddDevice} />
                    </View>
                </View>
                    {
                        this.state.isLoading ? <CustomLoader/> : null
                    }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerStyle: { position: 'absolute', top: 60, alignSelf: 'center', color: '#000000' },
    textInputStyle: { position: 'absolute', height: 40, width: 240, borderColor: 'gray', borderWidth: 1, top: 60 },
    noticeStyle: { position: 'relative', top: 60, alignSelf: 'center', color: '#000000' },
    addDeviceButtonStyle: { position: 'absolute', bottom: 100, alignSelf: 'center' },
    MainContainer: { flex: 1, backgroundColor: '#FFFFFF', },
});

