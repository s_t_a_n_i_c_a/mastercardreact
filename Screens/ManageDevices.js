'use strict';

import React from 'react';
import { StyleSheet, View, Text,Button, Image, FlatList,TouchableWithoutFeedback,Platform } from 'react-native';
import CustomLoader from '../Utils/CustomLoader'
import ManageDevicesService from '../Services/ManageDevicesService';
import EnrollmentService from '../Services/EnrollmentService';
import { EventRegister } from 'react-native-event-listeners'
import BaseService from './../Services/BaseService'
import { TENANT_CODE } from './../AppConstants';
import Account from "../Utils/Account";
import ReactNativeRouter from './../NativeModules';
import { NativeModules } from 'react-native';
export default class ManageDevices extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      devices: [],
      selectedDevices:[],
      isThisDeviceSelectedForDisabling:false
    };
  }

  componentWillMount() {
    this.listener = EventRegister.addEventListener('MCPayloadReceived', (messageData) => {
      EnrollmentService.setupPushNotificationPayload(messageData, (response) => {
      }, (error) => {
        alert(error);
      });
    })

    var reactNativeRouter = NativeModules.ReactNativeRouter;
    if (Platform.OS === 'ios') {
      reactNativeRouter.getNickname((nickname, error) => {
        Account.getSharedInstance().setNickname(nickname);
      });
    } else {
      ReactNativeRouter.getNickname((nickname) => {
        Account.getSharedInstance().setNickname(nickname);
      }, (error) => { });
    }
  }

  componentWillUnmount() {
    EventRegister.removeEventListener(this.listener)
  }
  componentDidMount = () => {
    
    this.setState({ isLoading: true });

    ManageDevicesService.initializeListDevices((responseString) => {
      this.callInitializeListDevicesAPI(responseString);
    }, (error) => {
      this.setState({ isLoading: false });
      alert(error)
    })

  }

  callInitializeListDevicesAPI(responseString) {

    this.setState({ isLoading: true });
    var headerParams = { 'Accept': 'application/json', 'Content-Type': 'application/json', 'charset': 'utf-8', 'tenantCode': TENANT_CODE };
    var bodyParams = responseString;
    
    new BaseService().request("mbPBES", "POST", headerParams, JSON.stringify({'payload': {'requestPBESstring':bodyParams}}), (serverResponse) => {

      ManageDevicesService.completeListDevices(JSON.stringify(JSON.parse(serverResponse).payload.responsePayload), (clientResponse) => {
        console.log('here')
        var obj = JSON.parse(clientResponse);
        this.setState({ isLoading: false, devices: obj.devices });

      }, (error) => {
        console.log('error initializing devices api', error)
        this.setState({ isLoading: false });
        alert(error)
      })


    }, (error) => {
      this.setState({ isLoading: false });
      alert(error);
    });
  }

  actionOnRow(item) {
    if(item.status === "DISABLED") 
    return;

    var selectedDevicesArray = this.state.selectedDevices.slice();    
    const exists = selectedDevicesArray.some(v => (v.deviceNickname === item.deviceNickname));

    if(exists){
      
      var count = -1; var index = 0;
      selectedDevicesArray.forEach(element => {
        count++;
        if (element.deviceNickname === item.deviceNickname){
          index = count;
        }
      });
      if (index !== -1) {
        selectedDevicesArray.splice(index, 1);
      }
      
    }else{
      selectedDevicesArray.push({"deviceNickname":item.deviceNickname,"status":item.status}); 
    }

    this.setState({selectedDevices:selectedDevicesArray})

 }
 disableDevices = () =>{
    const { selectedDevices } = this.state
    var selectedDevicesArray = [];

    selectedDevices.forEach(element => {
      selectedDevicesArray.push(element.deviceNickname);
      if (element.deviceNickname === Account.getSharedInstance().getNickname()){
        this.isThisDeviceSelectedForDisabling = true;
      }
    });
    
    if (selectedDevicesArray && selectedDevicesArray.length > 0){
      this.setState({ isLoading: true });
      ManageDevicesService.initializeDisableDevices(selectedDevicesArray,(responseString) => {
        this.callDisableDevicesAPI(responseString);
      }, (error) => {
        this.setState({ isLoading: false });
        alert(error)
      })
    }else{
      alert("Please select device(s) to disable.")
    }
 }

 callDisableDevicesAPI(responseString) {

  this.setState({ isLoading: true });
  var headerParams = { 'Accept': 'application/json', 'Content-Type': 'application/json', 'charset': 'utf-8', 'tenantCode': TENANT_CODE };
  var bodyParams = responseString;

  new BaseService().request("mbPBES", "POST", headerParams, JSON.stringify({'payload': {'requestPBESstring':bodyParams}}), (serverResponse) => {

    ManageDevicesService.completeDisableDevices(serverResponse, (clientResponse) => {
      alert(clientResponse);
      this.handleDisabledDevice()

      this.setState({ isLoading: false });

    }, (error) => {
      this.setState({ isLoading: false });
      alert(error)
    })


  }, (error) => {
    this.setState({ isLoading: false });
    alert(error);
  });
}
handleDisabledDevice = () =>{
  if(this.isThisDeviceSelectedForDisabling){
    ManageDevicesService.deleteAccount();
    this.props.navigation.replace('AddDevice')
  }else{
    this.props.navigation.pop()
  }
}
  render() {
    return (
      <View style={styles.container} >
        <FlatList
          data={this.state.devices}
          extraData={this.state}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) =>
            
          <TouchableWithoutFeedback onPress={ () => this.actionOnRow(item) }>

            <View style={styles.flatview}>
              <View style={styles.separator}>
                {
                  Account.sharedInstance.getNickname() == item.deviceNickname ?
                    <Text style={styles.textViewHighlighted}>{item.deviceNickname}</Text> :
                    <Text style={styles.textView}>{item.deviceNickname}</Text>
                }
                
                {
                  this.state.selectedDevices.some(v => (v.deviceNickname === item.deviceNickname))
                  ?
                  <Image
                  style={{ width: 12, height: 12, alignSelf: 'flex-end', marginRight: 10 }}
                  source={require('./../Images/check.png')}/> : null 
                }
                
                
                
                <Text style={styles.textView}>{item.status}</Text>
              </View>
            </View>

          </TouchableWithoutFeedback>

          }
          keyExtractor={item => item.status}


        />
        <View style={styles.ButtonStyle3}>
          <Button
            title="Disable Device(s)"
            color="#fe8f0a"
            onPress={this.disableDevices}
          />
        </View>
        {
          this.state.isLoading ? <CustomLoader /> : null
        }
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0,
    justifyContent: 'center',
    backgroundColor: '#FFF',
  },
  separator: {
    flex: 1,
    borderWidth: 1,
    borderColor: 'gray'
  },
  flatview: {
    marginTop: -1,
    backgroundColor: '#FFF',

  },
  textView: {
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 10,
  },
  textViewHighlighted: {
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 10,
    color: 'orange'
  },
  ButtonStyle3: {position: 'absolute', bottom: 40, alignSelf: 'center'},

});