package com.mastercard.identitycheck.ReactNativeRouter;

import android.app.Activity;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;
import com.mastercard.biometrics.AccountsController;
import com.mastercard.biometrics.IDCLifecycleObserver;
import com.mastercard.biometrics.IdCheckConfig;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.mastercard.biometrics.DeviceRegistrationListener;
import com.mastercard.biometrics.IdCheckData;
import com.mastercard.biometrics.IdCheckError;
import com.mastercard.biometrics.IdCheckService;
import com.mastercard.biometrics.listeners.IdcFidoProcessListener;
import com.mastercard.biometrics.models.AssuranceLevel;
import com.mastercard.identitycheck.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.annotations.NonNull;

public class ReactNativeRouter extends ReactContextBaseJavaModule {

    private static String TAG = "ReactNativeRouter";
    IdCheckService idCheckService = null;
    ReactApplicationContext context;

    public ReactNativeRouter(ReactApplicationContext reactContext) {

        super(reactContext);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new IDCLifecycleObserver());
        context = reactContext;
        idCheckService = IdCheckService.getInstance(reactContext);

    }

    @Override
    public String getName() {
        return "ReactNativeRouter";
    }

    @ReactMethod
    public void setMISUrl(String url) {
        IdCheckConfig.setMisServerUrl(url);
    }

    @ReactMethod
    public void initializeDeviceRegistrationWithCompletionHandler(Callback successBlock, Callback errorBlock) {
        try {
            IdCheckData idCheckData = idCheckService.initializeDeviceRegistration();
            if (idCheckData.hasError()) {

                String errorCode = String.valueOf(idCheckData.getError().getErrorCode());
                String errorMessage = String.valueOf(idCheckData.getError().getErrorMessage());
                if (TextUtils.isEmpty(errorMessage)) {
                    errorMessage = idCheckData.getError().getErrorMessage();
                }
                IdCheckError mError = new IdCheckError(errorCode, errorMessage);
                errorBlock.invoke(mError.getErrorMessage());
            } else {
                successBlock.invoke(idCheckData.getSdkData());
            }
        } catch (Exception e) {
            errorBlock.invoke(e.getMessage());
        }
    }


    @ReactMethod
    public void completeDeviceRegistrationForAccount(String tenantAccountId, String responseString, Callback successBlock, Callback errorBlock) {
        try {
            IdCheckData idCheckData = idCheckService.completeDeviceRegistration(tenantAccountId, responseString);
            if (idCheckData.hasError()) {

                String errorCode = String.valueOf(idCheckData.getError().getErrorCode());
                String errorMessage = String.valueOf(idCheckData.getError().getErrorMessage());
                if (TextUtils.isEmpty(errorMessage)) {
                    errorMessage = idCheckData.getError().getErrorMessage();
                }
                IdCheckError mError = new IdCheckError(errorCode, errorMessage);
                errorBlock.invoke(mError.getErrorMessage());
            } else {
                successBlock.invoke(idCheckData.getSdkData());
            }
        } catch (Exception e) {
            errorBlock.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void verifyDevice(String nickname, String verificationCode, final Callback successBlock, final Callback errorBlock) {
        try {
            idCheckService.verifyDevice(nickname, verificationCode, new DeviceRegistrationListener() {
                @Override
                public void onDeviceVerificationSuccess() {
                    successBlock.invoke();
                }

                @Override
                public void onDeviceVerificationFailure(IdCheckError error) {
                    errorBlock.invoke(error.getErrorMessage());
                }
            });

        } catch (Exception e) {
            errorBlock.invoke(e.getMessage());
        }
    }


    @ReactMethod
    public void initializeFidoRegistration(final Callback successBlock, final Callback failBlock) {

        Runnable r = new Runnable() {
            public void run() {

                try {
                    IdCheckData idCheckData = idCheckService.initializeFidoRegistration();
                    if (idCheckData.hasError()) {

                        String errorCode = String.valueOf(idCheckData.getError().getErrorCode());
                        String errorMessage = String.valueOf(idCheckData.getError().getErrorMessage());
                        if (TextUtils.isEmpty(errorMessage)) {
                            errorMessage = idCheckData.getError().getErrorMessage();
                        }
                        IdCheckError mError = new IdCheckError(errorCode, errorMessage);
                        failBlock.invoke(mError.getErrorMessage());
                    } else {
                        successBlock.invoke(idCheckData.getSdkData());
                    }
                } catch (Exception e) {
                    failBlock.invoke(e.getMessage());
                }
            }
        };

        new Thread(r).start();
    }

    @ReactMethod
    public void setupPushNotificationPayload(String channelId, String serverRandomData, String operation) {
        Bundle extras = new Bundle();
        extras.putString("channelId", channelId);
        extras.putString("serverRandomData", serverRandomData);
        extras.putString("operation", operation);
        idCheckService.setSDKPushPayload(extras);
    }

    @ReactMethod
    public void processFidoRegistration(final String encryptedString, final Callback successBlock, final Callback failBlock) {

        final MainActivity activity = MainActivity.getStaticInstance();
        if (!activity.isFinishing()) {
            try {

                idCheckService.processFidoRegistration(activity, encryptedString, idCheckData -> {
                    if (idCheckData.hasError()) {
                        failBlock.invoke(idCheckData.getError().getErrorMessage());

                    } else {
                        successBlock.invoke(idCheckData.getSdkData());
                    }
                });
            } catch (Exception e) {
                failBlock.invoke(e.getMessage());
            }
        } else {
            failBlock.invoke(new IdCheckError(404, "Activity Doesn't Exist"));
        }

    }

    @ReactMethod
    public void completeFidoRegistration(final String encryptedString, final Callback successBlock, final Callback failBlock) {

        final MainActivity activity = MainActivity.getStaticInstance();

        if (!activity.isFinishing()) {

            try {
                idCheckService.completeFidoRegistration(activity,encryptedString,idCheckData -> {
                    Log.d(TAG, "onFidoComplete: "+idCheckData.toString());
                    //Utils.hidePleaseWait();
                    if (idCheckData.hasError()) {
                        String errorCode = String.valueOf(idCheckData.getError().getErrorCode());
                        String errorMessage = String.valueOf(idCheckData.getError().getErrorMessage());
                        if (TextUtils.isEmpty(errorMessage)) {
                            errorMessage = idCheckData.getError().getErrorMessage();
                        }
                        IdCheckError mError = new IdCheckError(errorCode, errorMessage);
                        failBlock.invoke(mError.getErrorMessage());
                    } else {
                        Toast.makeText(activity, "Enrolled Successfully!", Toast.LENGTH_LONG).show();
                        successBlock.invoke(idCheckData.getSdkData());
                    }
                });
            } catch (Exception e) {
                failBlock.invoke(e.getMessage());
            }

        }else {
            failBlock.invoke(new IdCheckError(404, "Activity Doesn't Exist"));
        }
    }


    @ReactMethod
    public void initializeFidoAuthentication(final String transactionId, final String assuranceLevelString,final String transactionAuthType,final String auxiliaryData,final Callback successBlock, final Callback failBlock) {

        Runnable r = new Runnable() {
            public void run() {
                try {
                    AssuranceLevel assuranceLevel = AssuranceLevel.HIGH;
                    switch (assuranceLevelString){
                        case "high":   assuranceLevel = AssuranceLevel.HIGH;    break;
                        case "medium": assuranceLevel = AssuranceLevel.MEDIUM;  break;
                        case "low":    assuranceLevel = AssuranceLevel.LOW;     break;
                    }

                    IdCheckData idCheckData = idCheckService.initializeFidoAuthentication(transactionId, assuranceLevel, transactionAuthType, auxiliaryData);
                    if (idCheckData.hasError()) {

                        String errorCode = String.valueOf(idCheckData.getError().getErrorCode());
                        String errorMessage = String.valueOf(idCheckData.getError().getErrorMessage());
                        if (TextUtils.isEmpty(errorMessage)) {
                            errorMessage = idCheckData.getError().getErrorMessage();
                        }
                        IdCheckError mError = new IdCheckError(errorCode, errorMessage);
                        failBlock.invoke(mError.getErrorMessage());
                    } else {
                        successBlock.invoke(idCheckData.getSdkData());
                    }
                } catch (Exception e) {
                    failBlock.invoke(e.getMessage());
                }
            }
        };
        new Thread(r).start();
    }

    @ReactMethod
    public void processFidoAuthentication(String authenticationData, final Callback successBlock, final Callback failBlock) {

        final MainActivity activity = MainActivity.getStaticInstance();

        if (!activity.isFinishing()) {

            idCheckService.processFidoAuthentication(activity, authenticationData,new IdcFidoProcessListener() {
                @Override
                public void onBiometricsCaptured(IdCheckData idCheckData) {
                    Log.d(TAG, "onBiometricsCaptured: "+idCheckData.toString());

                    if (idCheckData.hasError()) {
                        failBlock.invoke(idCheckData.getError().getErrorMessage());
                    } else {
                        successBlock.invoke(idCheckData.getSdkData());
                    }
                }
            });
        }else {
            failBlock.invoke(new IdCheckError(404, "Activity Doesn't Exist"));
        }
    }

    @ReactMethod
    public void completeFidoAuthentication(String acknowledgmentResponse, Callback successBlock, Callback failBlock) {

        final MainActivity activity = MainActivity.getStaticInstance();

        if (!activity.isFinishing()) {
            try {

                idCheckService.completeFidoAuthentication(activity,acknowledgmentResponse,idCheckData -> {
                    Log.d(TAG, "onFidoComplete: "+idCheckData.toString());
                    if (idCheckData.hasError()) {
                        String errorCode = String.valueOf(idCheckData.getError().getErrorCode());
                        String errorMessage = String.valueOf(idCheckData.getError().getErrorMessage());
                        if (TextUtils.isEmpty(errorMessage)) {
                            errorMessage = idCheckData.getError().getErrorMessage();
                        }
                        IdCheckError mError = new IdCheckError(errorCode, errorMessage);
                        failBlock.invoke(mError.getErrorMessage());
                    } else {
                        Toast.makeText(activity, "Authenticated Successfully!", Toast.LENGTH_LONG).show();
                        successBlock.invoke(idCheckData.getSdkData());
                    }
                });
            } catch (Exception e) {
                failBlock.invoke(e.getMessage());
            }
        }else {
            failBlock.invoke(new IdCheckError(404, "Activity Doesn't Exist"));
        }
    }

    @ReactMethod
    public void initializeListDevices(final Callback successBlock, final Callback failBlock){
        Runnable r = new Runnable() {
            @Override
            public void run() {

        try{
            IdCheckData idCheckData = idCheckService.initializeListDevices();

            if (idCheckData.hasError()) {

                String errorCode = String.valueOf(idCheckData.getError().getErrorCode());
                String errorMessage = String.valueOf(idCheckData.getError().getErrorMessage());
                if (TextUtils.isEmpty(errorMessage)) {
                    errorMessage = idCheckData.getError().getErrorMessage();
                }
                IdCheckError mError = new IdCheckError(errorCode, errorMessage);
                failBlock.invoke(mError.getErrorMessage());
            } else {
                successBlock.invoke(idCheckData.getSdkData());
            }
        }catch (Exception e){
            failBlock.invoke(e.getMessage());
        }

            }
        };

        new Thread(r).start();

    }

    @ReactMethod
    public void completeListDevices(String listDevicesResponse,Callback successBlock, Callback failBlock){
        try{
            IdCheckData idCheckData = idCheckService.completeListDevices(listDevicesResponse);

            if (idCheckData.hasError()) {

                String errorCode = String.valueOf(idCheckData.getError().getErrorCode());
                String errorMessage = String.valueOf(idCheckData.getError().getErrorMessage());
                if (TextUtils.isEmpty(errorMessage)) {
                    errorMessage = idCheckData.getError().getErrorMessage();
                }
                IdCheckError mError = new IdCheckError(errorCode, errorMessage);
                failBlock.invoke(mError.getErrorMessage());
            } else {
                successBlock.invoke(idCheckData.getSdkData());
            }
        }catch (Exception e){
            failBlock.invoke(e.getMessage());
        }
    }


    @ReactMethod
    public void initializeDisableDevices(final ReadableArray deviceNicknames, final Callback successBlock, final Callback failBlock){


        Runnable r = new Runnable() {
            @Override
            public void run() {
                ArrayList<Object> arrayList =  deviceNicknames.toArrayList();
                String[] arr = new String[arrayList.size()];
                for(int i=0;i<arrayList.size();i++)
                {arr[i] = (String)arrayList.get(i);}

                try {
                    IdCheckData idCheckData = idCheckService.initializeDisableDevices(arr);
                    if (idCheckData.hasError()) {
                        String errorCode = String.valueOf(idCheckData.getError().getErrorCode());
                        String errorMessage = String.valueOf(idCheckData.getError().getErrorMessage());
                        if (TextUtils.isEmpty(errorMessage)) {
                            errorMessage = idCheckData.getError().getErrorMessage();
                        }
                        IdCheckError mError = new IdCheckError(errorCode, errorMessage);
                        failBlock.invoke(mError.getErrorMessage());
                    } else {
                        successBlock.invoke(idCheckData.getSdkData());
                    }
                } catch (Exception e) {
                    failBlock.invoke(e.getMessage());
                }
            }
        };
        new Thread(r).start();

    }

    @ReactMethod
    public void completeDisableDevices(String disableDevicesResponse,Callback successBlock, Callback failBlock){
        try{
            IdCheckData idCheckData = idCheckService.completeDisableDevices(disableDevicesResponse);
            if (idCheckData.hasError()) {
                String errorCode = String.valueOf(idCheckData.getError().getErrorCode());
                String errorMessage = String.valueOf(idCheckData.getError().getErrorMessage());
                if (TextUtils.isEmpty(errorMessage)) {
                    errorMessage = idCheckData.getError().getErrorMessage();
                }
                IdCheckError mError = new IdCheckError(errorCode, errorMessage);
                failBlock.invoke(mError.getErrorMessage());
            } else {
                successBlock.invoke(idCheckData.getSdkData());
            }
        }catch (Exception e){
            failBlock.invoke(e.getMessage());
        }
    }

    @ReactMethod
    public void deleteAccount() {
        idCheckService.deleteAccount();
    }


    @ReactMethod
    public void getAccountStatus(Callback successBlock, Callback failBlock) {
        if (AccountsController.getAccount() != null) {
            successBlock.invoke(AccountsController.getAccount().getStatus());
        } else {
            failBlock.invoke();
        }
    }

    @ReactMethod
    public void getNickname(Callback successBlock, Callback failBlock) {
        if (AccountsController.getAccount() != null) {
            successBlock.invoke(AccountsController.getAccount().getNickname());
        } else {
            failBlock.invoke();
        }
    }
}
